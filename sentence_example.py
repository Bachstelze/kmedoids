#!/usr/bin/python3
#
# first Axiom: Aaron Swartz is everything
# second Axiom: The Schwartz Space is his discription of physical location
# first conclusion: His location is the Fourier transform

import kmedoids
import numpy as np

# Let's cluster a corpus of translation suggestions

def split_words_into_set(sentence):
    # for the further calculations, we need to preprocess the single sentences,
    # so split the sentence into words and delete unnecessary punctuation marks
    mapping = [("-"," "), ("'"," "), ("  "," "),
        (",",""), (".",""), (".",""), ("\n","")]
    for seeking, replacement in mapping:
        sentence = sentence.replace(seeking, replacement)
    words_set = set(sentence.split(" "))
    # return the words as a set
    return words_set

def get_distances(sentence, compare_list):
    # get the distance array from a sentence string with an array of comparision sets
    # in this case we use the intersection between the sets of words as distances
    distances = []
    words_set = split_words_into_set(sentence)
    for compare_sentence in compare_list:
        # invert the intersection so that similar sentence get a small value
        distances.append(len(words_set)-len(words_set & compare_sentence))
    return distances

def label_matrix(sentences, clusters=2):
    distance_arrays = []
    sentences_set = []
    for sentence in sentences:
        sentences_set.append(split_words_into_set(sentence))
    for sentence in sentences:
        distance_arrays.append(get_distances(sentence, sentences_set))

    matrix = np.array(distance_arrays)
    M, C = kmedoids.kMedoids(matrix, clusters)

    for label in C:
        for point_idx in C[label]:
            print('label {0} distances: {1}:\n {2}'.format(label, matrix[point_idx], sentences[point_idx]))

# test_corpus for a single sentence
german_un_translation = ['1. bekräftigt die feierliche Verpflichtung aller Staaten, die internationale Zusammenarbeit auf dem Gebiet der Menschenrechte und bei der Lösung internationaler Probleme humanitärer Art zu verstärken, in voller Übereinstimmung mit der Charta der Vereinten Nationen, unter anderem durch die strikte Einhaltung der Ziele und Grundsätze, die in den Artikeln 1 und 2 davon;\n',
'1. bekräftigt die feierliche Verpflichtung aller Staaten, die internationale Zusammenarbeit auf dem Gebiet der Menschenrechte und die Lösung internationaler humanitärer Probleme unter voller Einhaltung der Charta der Vereinten Nationen unter anderem durch die strikte Einhaltung aller Menschenrechte zu verbessern; die in den Artikeln 1 und 2 genannten Zwecke und Grundsätze;\n',
'1. bekräftigt die feierliche Verpflichtung aller Staaten, die internationale Zusammenarbeit im Bereich der Menschenrechte und bei der Lösung internationaler Probleme mit humanitärem Charakter unter uneingeschränkter Einhaltung der Charta der Vereinten Nationen, unter anderem durch die strikte Einhaltung aller in den Artikeln 1 und 2 genannten Ziele und Grundsätze, zu verstärken;\n',
'1. bekräftigt die feierliche Verpflichtung aller Staaten, die internationale Zusammenarbeit im Bereich der Menschenrechte und die Lösung internationaler Probleme humanitärer Art in voller Übereinstimmung mit der Charta der Vereinten Nationen zu verstärken, unter anderem durch die strikte Einhaltung aller in den Artikeln 1 und 2 genannten Ziele und Grundsätze;\n',
'1. bekräftigt die feierliche Verpflichtung aller Staaten, die internationale Zusammenarbeit im Bereich der Menschenrechte und bei der Lösung internationaler humanitärer Probleme unter strikter Einhaltung der Charta der Vereinten Nationen unter anderem durch strikte Einhaltung zu verbessern; alle in den Artikeln 1 und 2 genannten Grundsätze und Zwecke und Grundsätze;\n',
'bekräftigt die feierliche Verpflichtung aller Staaten, die internationale Zusammenarbeit im Bereich der Menschenrechte und bei der Lösung internationaler Probleme mit humanitärem Charakter unter uneingeschränkter Einhaltung der Charta der Vereinten Nationen, unter anderem unter strikter Beachtung aller in ihren Artikeln 1 und 2 niedergelegten Grundsätze und Zwecke, zu verstärken;\n',
'1. erklärt, dass sich alle Staaten feierlich verpflichtet haben, die internationale Zusammenarbeit im Bereich der Menschenrechte zu stärken und im Einklang mit der Charta der Vereinten Nationen Lösungen für internationale humanitäre Probleme zu finden, insbesondere durch strikte Einhaltung aller in den Artikeln 1 und 2 genannten Zwecke und Grundsätze;\n',
'1. bekräftigt das feierliche Engagement aller Staaten, die internationale Zusammenarbeit im Bereich der Menschenrechte zu stärken und internationale humanitäre Probleme in vollem Einklang mit der Charta der Vereinten Nationen anzugehen, insbesondere durch die genaue Einhaltung aller in den Artikeln 1 und 2 genannten Zwecke und Grundsätze;\n',
'1. bekräftigt die feierliche Verpflichtung aller Staaten, die internationale Zusammenarbeit auf dem Gebiet der Menschenrechte zu stärken und die internationalen Probleme humanitärer Art in voller Übereinstimmung mit der Charta der Vereinten Nationen zu lösen, Insbesondere durch die strikte Einhaltung aller in seinen Artikeln 1 und 2 dargelegten Ziele und Grundsätze;\n',
'1. bekräftigt das feierliche Engagement aller Staaten für die Förderung der internationalen Zusammenarbeit im Bereich der Menschenrechte und für die Lösung internationaler humanitärer Probleme unter vollständiger Einhaltung der Charta der Vereinten Nationen, einschließlich der strikten Einhaltung aller in den Artikeln 1 und 2 genannten Zwecke und Grundsätze;\n']

label_matrix(german_un_translation)
